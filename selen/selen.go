package selen

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"unicode"

	"github.com/PuerkitoBio/goquery"
	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
	rep "gitlab.com/nichego.takogo.azaza/go-parser/repository"
)

const maxTries = 15

func Selen(w http.ResponseWriter, r *http.Request, query string, countVac int) []rep.Vacancy {
	var err error
	// прописываем конфигурацию для драйвера
	caps := selenium.Capabilities{
		"browserName": "chrome",
	}

	// добавляем в конфигурацию драйвера настройки для chrome
	chrCaps := chrome.Capabilities{
		W3C: true,
	}
	caps.AddChrome(chrCaps)

	// переменная нашего веб драйвера
	var wd selenium.WebDriver
	// прописываем адрес нашего драйвера
	urlPrefix := selenium.DefaultURLPrefix
	urlPrefix = "http://selen:4444/wd/hub"
	// немного костылей чтобы драйвер не падал
	i := 1
	for i < maxTries {
		wd, err = selenium.NewRemote(caps, urlPrefix)
		if err != nil {
			log.Println(err)
			i++
			continue
		}
		break
	}
	// после окончания программы завершаем работу драйвера
	defer wd.Quit()

	// сразу обращаемся к странице с поиском вакансии по запросу
	page := 1 // номер страницы
	err = wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query))
	if err != nil {
		http.Error(w, "selenium error: "+err.Error(), http.StatusInternalServerError)
		return nil
	}
	elem, err := wd.FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		http.Error(w, "selenium error: "+err.Error(), http.StatusInternalServerError)
		return nil
	}
	vacancyCount, err := elem.Text()
	if err != nil {
		http.Error(w, "selenium error: "+err.Error(), http.StatusInternalServerError)
		return nil
	}
	var count string
	for _, v := range vacancyCount {
		if unicode.IsDigit(v) {
			count += string(v)
		}
	}
	vacancy, err := strconv.Atoi(count)
	if err != nil {
		http.Error(w, "selenium error: "+err.Error(), http.StatusInternalServerError)
		return nil
	}
	page--
	links := make([]string, 0, 64)
	for vacancy > countVac {
		page++
		fmt.Println("PAGE:", page)
		var c int
		err = wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query))
		if err != nil {
			http.Error(w, "selenium error: "+err.Error(), http.StatusInternalServerError)
			return nil
		}
		elems, err := wd.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
		if err != nil {
			http.Error(w, "selenium error: "+err.Error(), http.StatusInternalServerError)
			return nil
		}
		for _, v := range elems {
			link, err := v.GetAttribute("href")
			if err != nil {
				continue
			}
			c++
			links = append(links, link)
		}
		vacancy -= c
		fmt.Println("vacancies: left", vacancy, ", got", len(links))
	}
	ans := make([]rep.Vacancy, 0, 64)
	for i := 0; i < len(links); i++ {
		resp, err := http.Get("https://career.habr.com" + links[i])
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return nil
		}

		doc, err := goquery.NewDocumentFromReader(resp.Body)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return nil
		}
		resp.Body.Close()
		dd := doc.Find("script[type=\"application/ld+json\"]")
		if dd == nil {
			http.Error(w, "cant find vacancy's JSON", http.StatusInternalServerError)
			return nil
		}
		ss := dd.First().Text()
		var vac rep.Vacancy
		var jl1 rep.JobLocation
		json.Unmarshal([]byte(ss), &vac)
		vac.JobLocation = nil
		err = json.Unmarshal([]byte(ss), &jl1)
		if err != nil {
			var jl2 rep.JobLocation2
			err = json.Unmarshal([]byte(ss), &jl2)
			if err == nil {
				vac.JobLocation = jl2.JobLocation
			}
		} else {
			vac.JobLocation = jl1.JobLocation
		}
		vac.ID = vac.Identifier.Value
		ans = append(ans, vac)
	}
	js, err := json.Marshal(ans)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	w.Write(js)
	return ans
}
