FROM golang
WORKDIR /app
COPY . .
RUN go build -o main main.go
CMD ["/app/main"]
EXPOSE 8080