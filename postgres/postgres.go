package postgres

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	rep "gitlab.com/nichego.takogo.azaza/go-parser/repository"
)

type helper struct {
	ID                 string `json:"id"`
	Context            string `json:"@context"`
	Type               string `json:"@type"`
	DatePosted         string `json:"datePosted"`
	Title              string `json:"title"`
	Description        string `json:"description"`
	Identifier         []byte `json:"identifier"`
	ValidThrough       string `json:"validThrough"`
	HiringOrganization []byte `json:"hiringOrganization"`
	JobLocation        []byte `json:"jobLocation"`
	JobLocationType    string `json:"jobLocationType"`
	EmploymentType     string `json:"employmentType"`
}

func init() {
	for i := 0; i < 10; i++ {
		time.Sleep(time.Second * 4)
		db, err := sqlx.Connect("postgres", "postgres://postgres:pass@post:5432/postgres?sslmode=disable")
		if err != nil {
			fmt.Printf("Try %d/10: %s\n", i+1, err)
			continue
		}
		defer db.Close()
		i += 10
		fmt.Println("Connected!")
		_, err = db.Queryx("SELECT id FROM vacancies")
		if err != nil {
			_, err = db.Queryx(`CREATE TABLE "vacancies" (
			"id" character varying(20) NOT NULL,
			PRIMARY KEY ("id"),
			"context" character varying(70) NULL,
			"type" character varying(30) NULL,
			"dateposted" character varying(20) NULL,
			"title" character varying(50) NULL,
			"description" character varying(6000) NULL,
			"identifier" json NULL,
			"validthrough" character varying(20) NULL,
			"hiringorganization" json NULL,
			"joblocation" json NULL,
			"joblocationtype" character varying(30) NULL,
			"employmenttype" character varying(30) NULL
		  );`)
			if err != nil {
				fmt.Println(err)
			}
		}
	}
}

type Post struct {
	Login    string
	Password string
	Path     string
	Port     string
	DB       string
	Table    string
}

func NewPost() *Post {
	return &Post{
		Login:    "postgres",
		Password: "pass",
		Path:     "post",
		DB:       "/postgres",
		Table:    "vacancies",
	}
}

func (v *Post) InsertMany(ans []rep.Vacancy) {
	db, err := sqlx.Connect("postgres", "postgres://"+v.Login+":"+v.Password+"@"+v.Path+v.DB+"?sslmode=disable")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer db.Close()
	selected, err := db.Queryx("SELECT id FROM " + v.Table)
	if err != nil {
		fmt.Println(err)
	}
	defer selected.Close()
	slc := make([]string, 0, 50)
	for selected.Next() {
		scn, err := selected.SliceScan()
		if err != nil {
			fmt.Println(err)
		}
		id := scn[0].(string)
		slc = append(slc, id)
	}
	for i := 0; i < len(ans); i++ {
		for _, v := range slc {
			if ans[i].ID == v {
				ans = append(ans[:i], ans[i+1:]...)
				i--
				break
			}
		}
	}
	for _, vv := range ans {
		identifier, err := json.Marshal(vv.Identifier)
		if err != nil {
			fmt.Println(err)
		}
		hiring, err := json.Marshal(vv.HiringOrganization)
		if err != nil {
			fmt.Println(err)
		}
		jobLocation, err := json.Marshal(vv.JobLocation)
		if err != nil {
			fmt.Println(err)
		}
		_, err = db.Queryx("INSERT INTO " + v.Table + " (id, context, type, dateposted, title, description, identifier, validthrough, hiringorganization, joblocation, joblocationtype, employmenttype) VALUES (" + fmt.Sprintf("'%v', '%v', '%v', '%v', '%v', '%v', '%v', '%v', '%v', '%v', '%v', '%v'", vv.ID, vv.Context, vv.Type, vv.DatePosted, vv.Title, vv.Description, string(identifier), vv.ValidThrough, string(hiring), string(jobLocation), vv.JobLocationType, vv.EmploymentType) + ")")
		if err != nil {
			fmt.Println(err)
		}
	}
}

func (v *Post) Get(id string) (ans rep.Vacancy, err error) {
	db, err := sqlx.Connect("postgres", "postgres://"+v.Login+":"+v.Password+"@"+v.Path+v.DB+"?sslmode=disable")
	if err != nil {
		return ans, err
	}
	defer db.Close()
	row := db.QueryRowx("SELECT * FROM " + v.Table + " WHERE id = '" + id + "'")
	if row.Err() != nil {
		return ans, err
	}
	var h helper
	err = row.StructScan(&h)
	if err != nil {
		return ans, err
	}
	json.Unmarshal(h.Identifier, &ans.Identifier)
	json.Unmarshal(h.HiringOrganization, &ans.HiringOrganization)
	ans.ID = h.ID
	ans.Context = h.Context
	ans.Type = h.Type
	ans.DatePosted = h.DatePosted
	ans.Title = h.Title
	ans.Description = h.Description
	ans.ValidThrough = h.ValidThrough
	ans.JobLocationType = h.JobLocationType
	ans.EmploymentType = h.EmploymentType
	var jl1 rep.JobLocation
	err = json.Unmarshal(h.JobLocation, &jl1.JobLocation)
	if err != nil {
		var jl2 rep.JobLocation2
		err = json.Unmarshal(h.JobLocation, &jl2.JobLocation)
		if err == nil {
			ans.JobLocation = jl2.JobLocation
		}
	} else {
		ans.JobLocation = jl1.JobLocation
	}
	return ans, nil
}

func (v *Post) Update(js string) (ans rep.Vacancy, err error) {
	db, err := sqlx.Connect("postgres", "postgres://"+v.Login+":"+v.Password+"@"+v.Path+v.DB+"?sslmode=disable")
	if err != nil {
		return ans, err
	}
	defer db.Close()
	err = json.Unmarshal([]byte(js), &ans)
	if err != nil {
		return ans, err
	}
	row := db.QueryRowx("SELECT * FROM " + v.Table + " WHERE id = '" + ans.ID + "'")
	if row.Err() != nil {
		return ans, err
	}
	var check rep.Vacancy
	err = row.StructScan(&check)
	if row.Err() != nil {
		return ans, err
	}
	if check.ID == "" {
		return ans, fmt.Errorf("there is no data with id %v", ans.ID)
	}
	identifier, err := json.Marshal(ans.Identifier)
	if err != nil {
		return ans, err
	}
	hiring, err := json.Marshal(ans.HiringOrganization)
	if err != nil {
		return ans, err
	}
	jobLocation, err := json.Marshal(ans.JobLocation)
	if err != nil {
		return ans, err
	}
	row = db.QueryRowx("UPDATE " + v.Table + " SET " + "context='" + ans.Context + "', type='" + ans.Type + "', dateposted='" + ans.DatePosted + "', title='" + ans.Title + "', description='" + ans.Description + "', identifier='" + string(identifier) + "', validthrough='" + ans.ValidThrough + "', hiringorganization='" + string(hiring) + "', joblocation='" + string(jobLocation) + "', joblocationtype='" + ans.JobLocationType + "', employmenttype='" + ans.EmploymentType + "' WHERE id = '" + ans.ID + "'")
	if row.Err() != nil {
		return ans, err
	}
	return ans, nil
}

func (v *Post) Delete(id string) error {
	db, err := sqlx.Connect("postgres", "postgres://"+v.Login+":"+v.Password+"@"+v.Path+v.DB+"?sslmode=disable")
	if err != nil {
		return err
	}
	defer db.Close()
	row := db.QueryRowx("SELECT * FROM " + v.Table + " WHERE id = '" + id + "'")
	if row.Err() != nil {
		return err
	}
	var check rep.Vacancy
	err = row.StructScan(&check)
	if row.Err() != nil {
		return err
	}
	if check.ID == "" {
		return fmt.Errorf("there is no data with id %v", id)
	}
	row = db.QueryRowx("DELETE FROM " + v.Table + " WHERE id = '" + id + "'")
	if row.Err() != nil {
		return err
	}
	return nil
}

func (v *Post) List() ([]rep.Vacancy, error) {
	db, err := sqlx.Connect("postgres", "postgres://"+v.Login+":"+v.Password+"@"+v.Path+v.DB+"?sslmode=disable")
	if err != nil {
		return nil, err
	}
	defer db.Close()
	rows, err := db.Queryx("SELECT * FROM " + v.Table)
	if err != nil {
		return nil, err
	}
	var h helper
	ans := make([]rep.Vacancy, 0, 50)
	for i := 0; rows.Next(); i++ {
		ans = append(ans, rep.Vacancy{})
		err = rows.StructScan(&h)
		if err != nil {
			return nil, err
		}
		json.Unmarshal(h.Identifier, &ans[i].Identifier)
		json.Unmarshal(h.HiringOrganization, &ans[i].HiringOrganization)
		ans[i].ID = h.ID
		ans[i].Context = h.Context
		ans[i].Type = h.Type
		ans[i].DatePosted = h.DatePosted
		ans[i].Title = h.Title
		ans[i].Description = h.Description
		ans[i].ValidThrough = h.ValidThrough
		ans[i].JobLocationType = h.JobLocationType
		ans[i].EmploymentType = h.EmploymentType
		var jl1 rep.JobLocation
		err = json.Unmarshal(h.JobLocation, &jl1.JobLocation)
		if err != nil {
			var jl2 rep.JobLocation2
			err = json.Unmarshal(h.JobLocation, &jl2.JobLocation)
			if err == nil {
				ans[i].JobLocation = jl2.JobLocation
			}
		} else {
			ans[i].JobLocation = jl1.JobLocation
		}
	}
	return ans, nil
}
