package repository

import (
	"fmt"
	"strconv"
	"sync"
)

type Vacancy struct {
	ID                 string             `json:"id" bson:"_id"`
	Context            string             `json:"@context"`
	Type               string             `json:"@type"`
	DatePosted         string             `json:"datePosted"`
	Title              string             `json:"title"`
	Description        string             `json:"description"`
	Identifier         Identifier         `json:"identifier"`
	ValidThrough       string             `json:"validThrough"`
	HiringOrganization HiringOrganization `json:"hiringOrganization"`
	JobLocation        interface{}        `json:"jobLocation"`
	JobLocationType    string             `json:"jobLocationType"`
	EmploymentType     string             `json:"employmentType"`
}

type Identifier struct {
	Type  string `json:"@type"`
	Name  string `json:"name"`
	Value string `json:"value"`
}

type HiringOrganization struct {
	Type   string `json:"@type"`
	Name   string `json:"name"`
	Logo   string `json:"logo"`
	SameAs string `json:"sameAs"`
}

type Address struct {
	Type            string `json:"@type"`
	StreetAddress   string `json:"streetAddress"`
	AddressLocality string `json:"addressLocality"`
	AddressCountry  struct {
		Type string `json:"@type"`
		Name string `json:"name"`
	} `json:"addressCountry"`
}

type JobLocation struct {
	JobLocation []struct {
		Type string `json:"@type"`
		Addr string `json:"address"`
	} `json:"jobLocation"`
}

type JobLocation2 struct {
	JobLocation struct {
		Type    string  `json:"@type"`
		Address Address `json:"address"`
	} `json:"jobLocation"`
}

type VacancyStorage struct {
	m map[int]Vacancy
	sync.Mutex
}

type Storager interface {
	Create(Vacancy) error
	GetByID(int) (Vacancy, error)
	GetList() []Vacancy
	Delete(int) error
}

func NewVacancyStorage() VacancyStorage {
	return VacancyStorage{
		m: make(map[int]Vacancy, 64),
	}
}

func (v *VacancyStorage) Create(vac Vacancy) error {
	v.Lock()
	defer v.Unlock()
	id, err := strconv.Atoi(vac.Identifier.Value)
	if err != nil {
		return fmt.Errorf("wrong id value: %s", vac.Identifier.Value)
	}
	if _, ok := v.m[id]; !ok {
		v.m[id] = vac
		return nil
	}
	return fmt.Errorf("vacancy already exist")
}

func (v *VacancyStorage) GetByID(id int) (Vacancy, error) {
	if m, ok := v.m[id]; ok {
		return m, nil
	}
	return Vacancy{}, fmt.Errorf("vacancy with this id doesn't exist")
}

func (v *VacancyStorage) GetList() []Vacancy {
	ans := make([]Vacancy, 0, 32)
	for _, v := range v.m {
		ans = append(ans, v)
	}
	return ans
}

func (v *VacancyStorage) Delete(id int) error {
	v.Lock()
	defer v.Unlock()
	if _, ok := v.m[id]; ok {
		delete(v.m, id)
		return nil
	}
	return fmt.Errorf("vacancy with this id doesn't exist")
}
