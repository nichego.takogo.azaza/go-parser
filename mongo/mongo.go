package mongo

import (
	"context"
	"encoding/json"
	"fmt"

	rep "gitlab.com/nichego.takogo.azaza/go-parser/repository"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Mong struct {
	Path       string
	DB         string
	Collection string
}

func NewMong() *Mong {
	return &Mong{
		Path:       "mong",
		DB:         "mydb",
		Collection: "vacancies",
	}
}

func (v *Mong) InsertMany(ans []rep.Vacancy) {
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://"+v.Path))
	if err != nil {
		fmt.Println(err)
		return
	}
	defer client.Disconnect(context.TODO())
	slc := make([]interface{}, 0, 50)
	for _, v := range ans {
		slc = append(slc, v)
	}
	collection := client.Database(v.DB).Collection(v.Collection)
	_, err = collection.InsertMany(context.TODO(), slc)
	if err != nil {
		fmt.Println(err)
	}
}

func (v *Mong) Get(id string) (ans rep.Vacancy, err error) {
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://"+v.Path))
	if err != nil {
		return ans, err
	}
	defer client.Disconnect(context.TODO())
	collection := client.Database(v.DB).Collection(v.Collection)
	founded := collection.FindOne(context.TODO(), bson.M{"_id": id})
	err = founded.Decode(&ans)
	if err != nil {
		return ans, err
	}
	ans.JobLocation = nil
	var jl1 rep.JobLocation
	err = founded.Decode(&jl1)
	if err != nil {
		var jl2 rep.JobLocation2
		err = founded.Decode(&jl2)
		if err == nil {
			ans.JobLocation = jl2.JobLocation
		}
	} else {
		ans.JobLocation = jl1.JobLocation
	}
	return ans, nil
}

func (v *Mong) Update(js string) (rep.Vacancy, error) {
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://"+v.Path))
	if err != nil {
		return rep.Vacancy{}, err
	}
	defer client.Disconnect(context.TODO())
	collection := client.Database(v.DB).Collection(v.Collection)
	var vac rep.Vacancy
	json.Unmarshal([]byte(js), &vac)
	var jl1 rep.JobLocation
	vac.JobLocation = nil
	err = json.Unmarshal([]byte(js), &jl1)
	if err != nil {
		var jl2 rep.JobLocation2
		err = json.Unmarshal([]byte(js), &jl2)
		if err == nil {
			vac.JobLocation = jl2.JobLocation
		}
	} else {
		vac.JobLocation = jl1.JobLocation
	}
	updated := collection.FindOneAndReplace(context.TODO(), bson.M{"_id": vac.ID}, vac)
	var vac2 rep.Vacancy
	err = updated.Decode(&vac2)
	if err != nil {
		return rep.Vacancy{}, err
	}
	return vac, nil
}

func (v *Mong) Delete(id string) error {
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://"+v.Path))
	if err != nil {
		return err
	}
	defer client.Disconnect(context.TODO())
	collection := client.Database(v.DB).Collection(v.Collection)
	del, err := collection.DeleteOne(context.TODO(), bson.M{"_id": id})
	if err != nil {
		return err
	}
	if del.DeletedCount == 0 {
		return fmt.Errorf("there is no data with id %s", id)
	}
	return nil
}

func (v *Mong) List() ([]rep.Vacancy, error) {
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://"+v.Path))
	if err != nil {
		return nil, err
	}
	defer client.Disconnect(context.TODO())
	collection := client.Database(v.DB).Collection(v.Collection)
	all, err := collection.Find(context.TODO(), bson.M{})
	if err != nil {
		return nil, err
	}
	ans := make([]rep.Vacancy, 0, 50)
	err = all.All(context.TODO(), &ans)
	if err != nil {
		return nil, err
	}
	return ans, nil
}
