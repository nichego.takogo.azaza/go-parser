package router

import (
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	env "github.com/Valgard/godotenv"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	_ "github.com/lib/pq"

	"gitlab.com/nichego.takogo.azaza/go-parser/mongo"
	"gitlab.com/nichego.takogo.azaza/go-parser/postgres"
	rep "gitlab.com/nichego.takogo.azaza/go-parser/repository"
	"gitlab.com/nichego.takogo.azaza/go-parser/selen"
)

const (
	swaggerTemplate = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-standalone-preset.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-standalone-preset.js"></script> -->
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-bundle.js"></script> -->
    <link rel="stylesheet" href="//unpkg.com/swagger-ui-dist@3/swagger-ui.css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui.css" /> -->
	<style>
		body {
			margin: 0;
		}
	</style>
    <title>Swagger</title>
</head>
<body>
    <div id="swagger-ui"></div>
    <script>
        window.onload = function() {
          SwaggerUIBundle({
            url: "/public/swagger.json?{{.Time}}",
            dom_id: '#swagger-ui',
            presets: [
              SwaggerUIBundle.presets.apis,
              SwaggerUIStandalonePreset
            ],
            layout: "StandaloneLayout"
          })
        }
    </script>
</body>
</html>
`
)

var DB_USE string

func swaggerUI(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	tmpl, err := template.New("swagger").Parse(swaggerTemplate)
	if err != nil {
		return
	}
	err = tmpl.Execute(w, struct {
		Time int64
	}{
		Time: time.Now().Unix(),
	})
	if err != nil {
		return
	}
}

func StartServer(port string) {
	err := env.Load(".env")
	if err != nil {
		fmt.Println("ERROR: cant read env file")
	} else {
		DB_USE = os.Getenv("DB_USE")
	}
	ctrl := NewController()
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	r.Post("/search", ctrl.Search)
	r.Post("/get", ctrl.Get)
	r.Put("/update", ctrl.Update)
	r.Post("/delete", ctrl.Delete)
	r.Get("/list", ctrl.List)

	//SwaggerUI
	r.Get("/swagger", swaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("/app/public"))).ServeHTTP(w, r)
	})

	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}
	// Запуск веб-сервера в отдельном горутине
	go func() {
		log.Println(fmt.Sprintf("server started on port %s ", port))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}

type DB interface {
	InsertMany([]rep.Vacancy)
	Get(string) (rep.Vacancy, error)
	Update(string) (rep.Vacancy, error)
	Delete(string) error
	List() ([]rep.Vacancy, error)
}

type Controller struct {
	DB DB
}

func NewController() Controller {
	return Controller{
		DB: NewStorage(),
	}
}

func NewStorage() DB {
	if DB_USE == "mongo" {
		fmt.Println("Using DB: Mongo")
		return mongo.NewMong()
	} else if DB_USE == "postgres" {
		fmt.Println("Using DB: Postgres")
		return postgres.NewPost()
	}
	fmt.Println("Using DB: Nothing")
	return nil
}

func (v *Controller) Search(w http.ResponseWriter, r *http.Request) {
	query := read(r.Body)
	query = query[7:]
	ans := selen.Selen(w, r, query, 160)
	v.DB.InsertMany(ans)
}

func (v *Controller) Get(w http.ResponseWriter, r *http.Request) {
	query := read(r.Body)
	query = query[3:]
	vac, err := v.DB.Get(query)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
	}
	js, err := json.Marshal(vac)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	w.Write(js)
}

func (v *Controller) Update(w http.ResponseWriter, r *http.Request) {
	query := read(r.Body)
	vac, err := v.DB.Update(query)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
	}
	js, err := json.Marshal(vac)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	w.Write(js)
}

func (v *Controller) Delete(w http.ResponseWriter, r *http.Request) {
	query := read(r.Body)
	query = query[3:]
	err := v.DB.Delete(query)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
}

func (v *Controller) List(w http.ResponseWriter, r *http.Request) {
	ans, err := v.DB.List()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	js, err := json.Marshal(ans)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	w.Write(js)
}

func read(r io.ReadCloser) (query string) {
	var err error
	var n int
	b := make([]byte, 16)
	for err == nil {
		n, err = r.Read(b)
		query = query + string(b[:n])
	}
	return
}
