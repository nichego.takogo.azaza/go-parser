package main

import (
	"gitlab.com/nichego.takogo.azaza/go-parser/router"
)

func main() {
	router.StartServer(":8080")
}
